from enum import Enum,auto
import shlex
import os
# pip install typeguard
from typeguard import typechecked
from itertools import combinations

class Ui:
	def __init__(self):
		self.queue=[]
		self.pt=PieceTable("0123456789")
		self.msgq=[]
		self.pt.msg=lambda msg:self.msgq.append(msg)
		# self.pt.msg=print
		self.pt.actstack.msg=lambda msg:self.msgq.append(msg)

		self.act={
			Ui.Cmd.none:lambda _:None,
			Ui.Cmd.insert:lambda pt:self.insert(pt),
			Ui.Cmd.remove:lambda pt:self.remove(pt),
			Ui.Cmd.exit:lambda _:None,
			Ui.Cmd.undo:lambda pt:pt.undo(),
			Ui.Cmd.redo:lambda pt:pt.redo(),
		}

	def start(self):
		cmd=Ui.Cmd.none
		# print("! "+Ui.Cmd.all())
		while not cmd==Ui.Cmd.exit:
			# cmd=Ui.Cmd(self.get_input("? what to do: "))
			os.system("cls")
			print(
				str(self.pt)
				+"\n\n"
				+self.pt.pieces_str()
				+"\n\nOLD: "
				+self.pt.file[Source.Old]
				+"\nNEW: "
				+self.pt.file[Source.New]
				+"\n\n"
				+str(self.pt.actstack)
				+"\n"
			)
			if len(self.msgq)>0:
				errs="\n - ".join(self.msgq)
				print(f" - {errs}\n")
				self.msgq=[]
			cmd=Ui.Cmd(self.get_input(""))
			# print("! "+str(cmd))
			# return
			self.act[cmd](self.pt)


	def insert(self,pt):
		index=int(self.get_input("? index: "))
		content=self.get_input("? content: ").replace("\\n","\n")
		self.pt.insert(content,index)

	def remove(self,pt):
		start=int(self.get_input("? index start: "))
		stop=int(self.get_input("? index stop: "))
		self.pt.remove(start,stop)


	class Cmd(Enum):
		none=""
		insert="i"
		remove="d"
		exit="x"
		undo="u"
		redo="r"
		# substring="s"
		# pieces="p"

		def all():
		# def __str__():
			res=[]
			for data in Ui.Cmd:
				if not data.name=="none":
					res.append(f"{data.name}: {data.value}")
				# res.append(f"{data}")
			return ", ".join(res)


	def get_input(self,msg):
		if len(self.queue)==0:
			inp=input(msg)
			self.queue=shlex.split(inp)
			if len(self.queue)==0:
				self.queue=[""]

		return self.queue.pop(0)


def substrings(text):
	for count in range(1,len(text)+1):
		combs=combinations(text,count)
		for comb in combs:
			yield "".join(comb)

def main():

	# p1=Piece(FPos(0),FLen(3),Source.New,Nlc(0))
	# p2=Piece(FPos(0),FLen(3),Source.New,Nlc(0))
	# ap1=ActivePiece(p1)
	# ap2=ActivePiece(p2)
	# ap1.next=ap2
	# ap1.next=p1

	return
	ui=Ui()
	ui.start()
# main()
# exit()

# def new_type(new,old):
# 	exec(f"class {new}({old}):\n\tdef __new__(cls,value):return super().__new__(cls,value)",globals())

# new_type("Idx","int")
# new_type("PIdx","Idx")
# new_type("Pos","int")
# new_type("Len","int")
# new_type("FPos","Pos")
# new_type("FLen","Len")
# new_type("TPos","Pos")
# new_type("TLen","Len")
# new_type("PPos","Pos")

	# pos
	# len
	#
	# start
	# stop - not inclusive
	#
	# first
	# last - inclusive
	#
class Nlc(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Idx(int):
	def __new__(cls,value):return super().__new__(cls,value)
class PIdx(Idx):
	def __new__(cls,value):return super().__new__(cls,value)
class Pos(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Len(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Row(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Col(int):
	def __new__(cls,value):return super().__new__(cls,value)

class First(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Last(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Start(int):
	def __new__(cls,value):return super().__new__(cls,value)
class Stop(int):
	def __new__(cls,value):return super().__new__(cls,value)


class FPos(Pos):
	def __new__(cls,value):return super().__new__(cls,value)
class FLen(Len):
	def __new__(cls,value):return super().__new__(cls,value)
class TPos(Pos):
	def __new__(cls,value):return super().__new__(cls,value)
class TLen(Len):
	def __new__(cls,value):return super().__new__(cls,value)
class PPos(Pos):
	def __new__(cls,value):return super().__new__(cls,value)


# ██████╗ ██╗███████╗ ██████╗███████╗██╗███╗   ██╗███████╗ ██████╗
# ██╔══██╗██║██╔════╝██╔════╝██╔════╝██║████╗  ██║██╔════╝██╔═══██╗
# ██████╔╝██║█████╗  ██║     █████╗  ██║██╔██╗ ██║█████╗  ██║   ██║
# ██╔═══╝ ██║██╔══╝  ██║     ██╔══╝  ██║██║╚██╗██║██╔══╝  ██║   ██║
# ██║     ██║███████╗╚██████╗███████╗██║██║ ╚████║██║     ╚██████╔╝
# ╚═╝     ╚═╝╚══════╝ ╚═════╝╚══════╝╚═╝╚═╝  ╚═══╝╚═╝      ╚═════╝




# class PieceInfo:
# 	# pIdx fPos fLen Src Nlc
# 	#  i    p    l   s   n

# 	ATTR_FROM_CHAR={
# 		"i":"pidx",
# 		"p":"fpos",
# 		"l":"flen",
# 		"s":"src",
# 		"n":"nlc",
# 	}

# 	# def __init__(self,pinf,name):
# 		# self.__init__(pinf.pidx,pinf.fpos,pinf.flen,pinf.src,pinf.nlc)

# 	def __init__(self,pinf=None,name=None,pidx=None,fpos=None,flen=None,src=None,nlc=None):

# 		if pinf is not None:
# 			self._pidx=pinf.pidx
# 			self._fpos=pinf.fpos
# 			self._flen=pinf.flen
# 			self._src=pinf.src
# 			self._nlc=pinf.nlc
# 		else:
# 			self._pidx=pidx
# 			self._fpos=fpos
# 			self._flen=flen
# 			self._src=src
# 			self._nlc=nlc
# 		if name is not None:
# 			for char in name.split("_")[1]:
# 				attr=PieceInfo.ATTR_FROM_CHAR[char]
# 				if getattr(self,attr) is None:
# 					msg=f"{name}: {attr} is None."
# 					# raise TypeError(msg)
# 					print("xx "+msg)

# 	def set_pidx(self,new_pidx):
# 		self._pidx=new_pidx
# 		return self

# 	@property
# 	def pidx(self): return self._pidx
# 	@property
# 	def fpos(self): return self._fpos
# 	@property
# 	def flen(self): return self._flen
# 	@property
# 	def src(self): return self._src
# 	@property
# 	def nlc(self): return self._nlc

# 		# if self.pidx is None:
# 			# raise TypeError("PieceInfo_i: pidx is None.")
# 			# print("xx PieceInfo_i: pidx is None.")

# 	# def assert_some(self,cmd):
# 	# 	for ch in "iplsn":
# 	# 		if ch in cmd:
# 	# 			assert self.pidx is not None,f"PieceInfo: \"{ch}\" is None"

# def new_pieceinfo(chars):
# 	exec(f"class PieceInfo_{chars}(PieceInfo):\n\tdef __init__(self,pinf):super().__init__(pinf=pinf,name=type(self).__name__)",globals())


# for substr in substrings("iplsn"):
# 	new_pieceinfo(substr)
# #




# ██████╗ ██╗███████╗ ██████╗███████╗████████╗ █████╗ ██████╗ ██╗     ███████╗
# ██╔══██╗██║██╔════╝██╔════╝██╔════╝╚══██╔══╝██╔══██╗██╔══██╗██║     ██╔════╝
# ██████╔╝██║█████╗  ██║     █████╗     ██║   ███████║██████╔╝██║     █████╗
# ██╔═══╝ ██║██╔══╝  ██║     ██╔══╝     ██║   ██╔══██║██╔══██╗██║     ██╔══╝
# ██║     ██║███████╗╚██████╗███████╗   ██║   ██║  ██║██████╔╝███████╗███████╗
# ╚═╝     ╚═╝╚══════╝ ╚═════╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚══════╝

	# things that change the text view without changing text:
	# tabwidth, word wrap (ww), word wrap indent.


	# The main entities are:
	# - buffer: text in memory (potentially) associated with a file on disk. Realized via a piecetable.
	# - view: Part of a buffer and maybe some regions visualized.
	# - region(/cursor): Regions of the buffer where manipulation of the buffer happens.
	# Multiple regions and views can be independently associated with a buffer.

	# A view may have to different modes: word wrap (ww) or rectangular (rect).
	# if rect:
	#  The view is a rectangle (row,col,drow,dcol).
	#  If the main cursor moves, the view gets set such that the cursor inside the view (+ some padding).
	# if ww:
	#  The view is a location (row,col) together with num char behind and after
	#
	# The buffer must offer the following functions:
	#  get_substring(pos,len)
	#  get_substring(row_first,row_last)
	#  insert(pos,text)
	#  insert(row,col,text)
	#  remove(pos,len)
	#  remove(row,col,len)
	#  undo()
	#  redo()
	#
	#  A "position" describes one character in the buffer by its index.
	#  A "location" describes one character in the buffer by its row and column.
	#
	#  pos=start=first
	#  stop=last+1=pos+len


	# Done:
	# - remove
	# - handle empty
	# - undo groups
	# Todo:
	# - add pinf and nlc in MANIP 0
	# - lines
	# - cursors
	# - view
	# - test suite
	# - benchmarking to compare different versions
	# - optimization
	# -- delete multiple pieces

# def unpack(obj):
# 	return obj.pidx,obj.new_fpos,obj.new_flen,obj.new_src,obj.nlc


def nonstr(obj):
	return "" if obj is None else str(obj)

class PieceTable:

	class Caret:
		def __init__(self,pt,location):


#            █████╗  ██████╗████████╗██╗ ██████╗ ███╗   ██╗
#           ██╔══██╗██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║
# █████╗    ███████║██║        ██║   ██║██║   ██║██╔██╗ ██║
# ╚════╝    ██╔══██║██║        ██║   ██║██║   ██║██║╚██╗██║
#           ██║  ██║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║
#           ╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝

	# class Action:
	# 	def __init__(self,fun,pinf):
	# 		self.pinf=pinf
	# 		self.fun=fun


	# 	def __str__(self):
	# 		return (
	# 			('@'+nonstr(self.pinf.pidx)).rjust(3)
	# 			+" "
	# 			+type(self).__name__[6:9].upper()
	# 			+" "
	# 			+nonstr(self.pinf.src).rjust(3)
	# 			+" ("
	# 			+nonstr(self.pinf.fpos).rjust(4)
	# 			+nonstr(self.pinf.flen).rjust(4)
	# 			+nonstr(self.pinf.nlc).rjust(4)
	# 			+")"
	# 		)


	# 	def __call__(self):
	# 		self.fun(self.pinf)

#           ██╗███╗   ██╗██╗████████╗
#           ██║████╗  ██║██║╚══██╔══╝
# █████╗    ██║██╔██╗ ██║██║   ██║
# ╚════╝    ██║██║╚██╗██║██║   ██║
#           ██║██║ ╚████║██║   ██║
#           ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝

	def __init__(self,initial_text=None):
		self.msg=lambda txt:None
		self.actstack=ActionStack()

		if initial_text is None:
			initial_text=""

		self.file={
			Source.Old:initial_text,
			Source.New:""
		}

		self.length=len(initial_text)
		self.nlc=self.get_nlc(initial_text)

		if self.length>0:
			self.pt_head=Piece(
				fpos=FPos(0),
				flen=Flen(self.length),
				src=Source.Old,
				nlc=self.nlc
			)
			self.pt_len=1
		else:
			self.pt_head=None
			self.pt_len=0

	def get_nlc(self,text):
		return Nlc(text.count("\n"))

	def __str__(self):
		return self.substr(0,self.length)

	def  __len__(self):
		return self.length


#           ███╗   ███╗ █████╗ ███╗   ██╗██╗██████╗      ██████╗
#           ████╗ ████║██╔══██╗████╗  ██║██║██╔══██╗    ██╔═████╗
# █████╗    ██╔████╔██║███████║██╔██╗ ██║██║██████╔╝    ██║██╔██║
# ╚════╝    ██║╚██╔╝██║██╔══██║██║╚██╗██║██║██╔═══╝     ████╔╝██║
#           ██║ ╚═╝ ██║██║  ██║██║ ╚████║██║██║         ╚██████╔╝
#           ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝          ╚═════╝
	@typechecked
	def insert(self,content:str,loc:Location):
		assert content is not None,f"PieceTable.insert: content is empty."
		content_len=len(content)
		if content_len==0:
			return
		# assert 0<=tpos<=self.length,f"PieceTable.insert: tpos ({tpos}) is not in [0,{self.length}]."


		nflen_prev=len(self.file[Source.New])
		nflen_curr=nflen_prev+content_len
		self._append_to_file(content)

		fpos=nflen_prev

		with self.actstack:
			if self.pt_head is None:
				self.msg(f"PieceTable.insert: add first piece.")
				self.add_piece_action(0,fpos,content_len,Source.New)
			else:

				if tpos==self.length:
					self.msg("PieceTable.insert: insert at end.")
					last_pidx=self.pt_len-1
					last_piece=self.get_piece(last_pidx)
					if last_piece.src==Source.New and last_piece.fpos+last_piece.flen==fpos:
						self.msg(f"PieceTable.insert: extend last piece by {content_len}.")
						self.change_piece_action(last_pidx,last_piece.fpos,last_piece.flen+content_len)
					else:
						self.msg(f"PieceTable.insert: add piece at the end.")
						self.add_piece_action(last_pidx+1,fpos,content_len,Source.New)
				else:
					spot=self.get_spots([tpos])[0]
					piece=self.get_piece(spot.pidx)
					if (spot.pidx,spot.ppos)==(0,0):
						self.msg("PieceTable.insert: insert at start.")
						self.add_piece_action(0,fpos,content_len,Source.New)
					elif spot.ppos==0:
						self.msg("PieceTable.insert: insert before piece.")
						prev_pidx=spot.pidx-1
						prev_piece=piece.prev
						self.msg("PieceTable.insert: check if piece before contains EOF.")
						if prev_piece.src==Source.New and prev_piece.fpos+prev_piece.flen==fpos:
							self.msg(f"PieceTable.insert: extend piece {prev_pidx} by {content_len}.")
							self.change_piece_action(prev_pidx,prev_piece.fpos,prev_piece.flen+content_len)
						else:
							self.msg(f"PieceTable.insert: add piece at index {prev_pidx+1}.")
							self.add_piece_action(prev_pidx+1,fpos,content_len,Source.New)
					else:
						self.msg("PieceTable.insert: insert inside piece.")
						# print(f"xx {spot}")
						second_half_piece=(
							piece.fpos+spot.ppos,
							piece.flen-spot.ppos,
							piece.src)
						self.change_piece_action(spot.pidx,piece.fpos,spot.ppos)
						self.add_piece_action(spot.pidx+1,nflen_curr-content_len,content_len,Source.New)
						self.add_piece_action(spot.pidx+2,*second_half_piece)

		# for idx,action in enumerate(actions):
			# self.actstack.act(*action)
			# self.msg(f"x after act {idx+1}/{len(actions)}")
			# self.pr_pieces()
		# check possible different locations:
		# - start of text
		# -- add node at start of text
		# - inside piece P
		# -- reduce length of P to offset
		# -- insert new piece after P with new content
		# -- insert new piece from offset to end of prev.
		# - after piece P (P contains end of Source.New file)
		# -- extend length of P by length of insert
		# - after piece P (P does not contain end of Source.New file)
		# -- insert new piece after P with new content

	def remove(self,tstart,tstop):

		tfirst=tstart
		tlast=tstop-1
		spot_first,spot_last=self.get_spots([tfirst,tlast])

		with self.actstack:
			if spot_first.pidx==spot_last.pidx:
				piece=self.get_piece(spot_first.pidx)
				pidx=spot_first.pidx
				ppos_start=spot_first.ppos
				ppos_stop=spot_last.ppos+1

				# if at end: reduce flen
				# if complete piece: remove piece
				# if in middle: reduce flen, add piece
				# if at start: increase fpos, reduce flen

				if ppos_start==0:
					if ppos_stop==piece.flen:
						self.msg(f"PieceTable.remove: remove single whole piece {pidx}.")
						self.remove_piece_action(pidx)
					else:
						self.msg(f"PieceTable.remove: remove beginning of piece {pidx} ({ppos_stop} chars).")
						new_fpos=piece.fpos+ppos_stop
						new_flen=piece.flen-ppos_stop
						self.change_piece_action(pidx,new_fpos,new_flen)
				else:
					if ppos_stop==piece.flen:
						self.msg(f"PieceTable.remove: remove end of piece {pidx} ({piece.flen-ppos_stop} chars).")
						new_fpos=piece.fpos
						new_flen=ppos_stop # piece.flen-(piece.flen-ppos_stop)
						self.change_piece_action(pidx,new_fpos,new_flen)
					else:
						self.msg(f"PieceTable.remove: remove in the middle of piece {pidx} ({ppos_start} to {ppos_stop}, {ppos_stop-ppos_start} chars).")
						new_fpos1=piece.fpos
						new_flen1=ppos_start
						new_fpos2=piece.fpos+(ppos_stop)
						new_flen2=piece.flen-(ppos_stop)
						self.change_piece_action(pidx,new_fpos1,new_flen1)
						self.add_piece_action(pidx+1,new_fpos2,new_flen2,piece.src)
			else:

				piece_first,piece_last=self.get_pieces([spot_first.pidx,spot_last.pidx])

				fpos_f,flen_f,_=piece_first
				fpos_l,flen_l,_=piece_last
				pidx_f,ppos_f  =spot_first
				pidx_l,ppos_l  =spot_last

				ppos_start=ppos_f
				ppos_stop=ppos_l+1

				self.msg(f"PieceTable.remove: remove end of piece {pidx_f} ({flen_f-ppos_start} chars).")
				self.change_piece_action(pidx_f,fpos_f,ppos_start)
				offset=1
				# pidx_c=pidx_f+1
				while pidx_f+offset<pidx_l:
					self.msg(f"PieceTable.remove: remove whole piece {pidx_f+offset} ({offset}/{pidx_l-pidx_f-1}).")
					self.remove_piece_action(pidx_f+1)
					offset+=1

				self.msg(f"PieceTable.remove: remove beginning of piece {pidx_l} ({ppos_stop} chars).")
				self.change_piece_action(pidx_f+1,fpos_l+ppos_stop,flen_l-ppos_stop)




	def undo(self):
		self.actstack.undo()

	def redo(self):
		self.actstack.redo()

#           ███╗   ███╗ █████╗ ███╗   ██╗██╗██████╗      ██╗
#           ████╗ ████║██╔══██╗████╗  ██║██║██╔══██╗    ███║
# █████╗    ██╔████╔██║███████║██╔██╗ ██║██║██████╔╝    ╚██║
# ╚════╝    ██║╚██╔╝██║██╔══██║██║╚██╗██║██║██╔═══╝      ██║
#           ██║ ╚═╝ ██║██║  ██║██║ ╚████║██║██║          ██║
#           ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝          ╚═╝
	# def __init__(self,fpos:int,flen:int,src:Source):
	# @typechecked
	# def add_piece_action(self,pinf:PieceInfo_i):
	def add_piece_action(self,):
		self.actstack.act(
			redo=PieceTable.Action(self._add_piece,pinf),
			undo=PieceTable.Action(self._remove_piece,pinf)
		)

	# @typechecked
	def remove_piece_action(self,pinf:PieceInfo_i):
		self.actstack.act(
			redo=PieceTable.Action(self._remove_piece,pinf),
			undo=PieceTable.Action(self._add_piece,self.get_piece(pinf.pidx).pinf)
		)

	# @typechecked
	def change_piece_action(self,pinf:PieceInfo_i):
		self.actstack.act(
			redo=PieceTable.ActionChange(self._change_piece,pinf),
			undo=PieceTable.ActionChange(self._change_piece,self.get_piece(pinf.pidx).pinf)
		)

#           ███╗   ███╗ █████╗ ███╗   ██╗██╗██████╗     ██████╗
#           ████╗ ████║██╔══██╗████╗  ██║██║██╔══██╗    ╚════██╗
# █████╗    ██╔████╔██║███████║██╔██╗ ██║██║██████╔╝     █████╔╝
# ╚════╝    ██║╚██╔╝██║██╔══██║██║╚██╗██║██║██╔═══╝     ██╔═══╝
#           ██║ ╚═╝ ██║██║  ██║██║ ╚████║██║██║         ███████╗
#           ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝         ╚══════╝

	# def _add_piece(self,pinf:PieceInfo_iplsn):
	@typechecked
	def _add_piece(self,pasp:PassivePiece):

		new_piece=pasp.activate()

		if pasp.pidx==0:
			self.msg("PieceTable._add_piece: adding at the start.")
			second_piece=self.pt_head
			self.pt_head=new_piece
			self.pt_head.next=second_piece
			if second_piece is not None:
				second_piece.prev=self.pt_head
		elif pasp.pidx==self.pt_len:
			self.msg("PieceTable._add_piece: adding at the end.")
			# OPTIMIZE
			second_last_piece=self.get_piece(pasp.pidx)
			new_piece.prev=second_last_piece
			second_last_piece.next=new_piece
		else:
			self.msg(f"PieceTable._add_piece: adding in the middle (index {pasp.pidx}).")
			# OPTIMIZE
			after_piece=self.get_piece(pasp.pidx)
			before_piece=after_piece.prev

			before_piece.next=new_piece
			new_piece.prev=before_piece
			new_piece.next=after_piece
			after_piece.prev=new_piece
		self.length+=new_piece.flen
		self.nlc+=new_piece.nlc
		self.pt_len+=1

	@typechecked
	def _remove_piece(self,pidx:PIdx):
		# pinf.assert_some("i")

		if pidx==0:
			self.msg("PieceTable._remove_piece: removing at the start.")
			rem_piece=self.pt_head.flen
			self.pt_head=self.pt_head.next
		elif pidx==self.pt_len-1:
			self.msg("PieceTable._remove_piece: removing at the end.")
			# OPTIMIZE
			rem_piece=self.get_piece(pidx)
			rem_piece.prev.next=None
		else:
			self.msg(f"PieceTable._remove_piece: removing in the middle (index {pidx}).")
			# OPTIMIZE
			rem_piece=self.get_piece(pidx)
			before_piece=rem_piece.prev
			after_piece=rem_piece.next
			before_piece.next=after_piece
			after_piece.prev=before_piece
		self.length-=rem_piece.flen
		self.nlc-=rem_piece.nlc
		self.pt_len-=1

	def _change_piece(self,pasp:PassivePiece):
		# pinf.assert_some("ifl")

		target_piece=self.get_piece(pasp.pidx)

		self.msg(f"PieceTable._change_piece: fpos {target_piece.fpos}->{pasp.fpos}, flen {target_piece.flen}->{pasp.flen}")

		delta_len=pasp.flen-target_piece.flen
		delta_nlc=pasp.nlc-target_piece.nlc

		target_piece.pinf=pinf

		self.length+=delta_len
		self.nlc+=delta_nlc

#           ███████╗██╗   ██╗██████╗ ███████╗████████╗██████╗
#           ██╔════╝██║   ██║██╔══██╗██╔════╝╚══██╔══╝██╔══██╗
# █████╗    ███████╗██║   ██║██████╔╝███████╗   ██║   ██████╔╝
# ╚════╝    ╚════██║██║   ██║██╔══██╗╚════██║   ██║   ██╔══██╗
#           ███████║╚██████╔╝██████╔╝███████║   ██║   ██║  ██║
#           ╚══════╝ ╚═════╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝  ╚═╝

	def substr(self,tstart,tstop):

		if self.pt_head is None:
			return ""

		tfirst=tstart
		tlast=tstop-1
		spot_first,spot_last=self.get_spots([tfirst,tlast])

		piece_first,piece_last=self.get_pieces([spot_first.pidx,spot_last.pidx])

		len_including_ppos=spot_last.ppos+1

		slice_start=piece_first.pinf.fpos+spot_first.ppos
		slice_stop=piece_last.pinf.fpos+len_including_ppos

		involved_piece_count=spot_last.pidx-spot_first.pidx+1

		slices=[]

		if involved_piece_count==1:
			slices.append((slice_start,slice_stop,piece_first.pinf.src))
		else:
			slices.append((slice_start,piece_first.fstop(),piece_first.pinf.src))

			if involved_piece_count>2:
				curr_piece=piece_first.next
				curr_pidx=spot_first.pidx+1
				while curr_pidx<spot_last.pidx:
					# curr_piece spot_last
					slices.append((curr_piece.fpos,curr_piece.fpos+curr_piece.flen,curr_piece.src))
					curr_piece=curr_piece.next
					curr_pidx+=1

			slices.append((piece_last.pinf.fpos,slice_stop,piece_last.pinf.src))

		res=""
		for start,stop,src in slices:
			# print(sl)
			res+=self.file[src][slice(start,stop)]
		return res

#           ███████╗██╗   ██╗██████╗ ██████╗  ██████╗ ██████╗ ████████╗
#           ██╔════╝██║   ██║██╔══██╗██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝
# █████╗    ███████╗██║   ██║██████╔╝██████╔╝██║   ██║██████╔╝   ██║
# ╚════╝    ╚════██║██║   ██║██╔═══╝ ██╔═══╝ ██║   ██║██╔══██╗   ██║
#           ███████║╚██████╔╝██║     ██║     ╚██████╔╝██║  ██║   ██║
#           ╚══════╝ ╚═════╝ ╚═╝     ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝

	def get_piece(self,target_pidx):
		assert self.pt_head is not None, f"PieceTable.get_piece: piecetable is empty."

		curr_index=0
		curr_piece=self.pt_head
		while curr_index<target_pidx and curr_index<self.pt_len-1:
			curr_index+=1
			curr_piece=curr_piece.next

		# curr_piece.pinf.set_pidx(target_pidx)
		return curr_piece

	def get_pieces(self,sorted_pidxs):
		assert self.pt_head is not None, f"PieceTable.get_pieces: piecetable is empty."

		if len(sorted_pidxs)==0:
			return []
		found_pieces=[]
		source_index=0
		pidxs_count=len(sorted_pidxs)
		search_index=0
		search_piece=self.pt_head
		while source_index<pidxs_count and search_index<self.pt_len:
			if sorted_pidxs[source_index]==search_index:
				search_piece.pinf.set_pidx(search_index)
				found_pieces.append(search_piece)
				source_index+=1
			else:
				search_index+=1
				search_piece=search_piece.next
		assert source_index==pidxs_count,f"PieceTable.get_piece: did not find all pidx in {sorted_pidxs}. Either they are not sorted or some are not smaller than the piece count {self.pt_len}."
		return found_pieces

	def get_spots(self,sorted_tposs):
		spots=[]

		assert self.pt_head is not None, f"PieceTable.get_spots: piecetable is empty."

		curr_piece=self.pt_head
		curr_pidx=0

		# Notice: curr_piece_tstop==next_piece_tstart
		curr_piece_tstop=self.pt_head.flen

		for tpos in sorted_tposs:
			assert tpos<self.length,f"PieceTable.get_spots: tpos ({tpos}) is greater than last possible index in text ({self.length-1})."
			while tpos>=curr_piece_tstop:
				# curr_piece.next should exist in this case. Otherwise the piece table is malformed.
				curr_piece=curr_piece.next
				curr_piece_tstop+=curr_piece.pinf.flen
				curr_pidx+=1

			curr_piece_tstart=curr_piece_tstop-curr_piece.pinf.flen
			ppos=tpos-(curr_piece_tstart)

			spots.append(Spot(curr_pidx,ppos))

		return spots

	def pieces_str(self):
		pieces=[]
		piece=self.pt_head
		while piece is not None:
			pieces.append(str(piece))
			piece=piece.next
		return "\n".join(pieces)

	def _append_to_file(self,content):
		self.file[Source.New]+=content


# ███████╗ ██████╗ ██╗   ██╗██████╗  ██████╗███████╗
# ██╔════╝██╔═══██╗██║   ██║██╔══██╗██╔════╝██╔════╝
# ███████╗██║   ██║██║   ██║██████╔╝██║     █████╗
# ╚════██║██║   ██║██║   ██║██╔══██╗██║     ██╔══╝
# ███████║╚██████╔╝╚██████╔╝██║  ██║╚██████╗███████╗
# ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚══════╝


class Source(Enum):
	Old=0
	New=1

	def __str__(self):
		return self.name

# ██╗      ██████╗  ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
# ██║     ██╔═══██╗██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
# ██║     ██║   ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║
# ██║     ██║   ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
# ███████╗╚██████╔╝╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
# ╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝

class Spot:
	@typechecked
	def __init__(self,pidx:PIdx,ppos:PPos):
		assert pidx>=0,"Spot: pidx must be non negative"
		assert ppos>=0,"Spot: ppos must be non negative"
		self._pidx=pidx
		self._ppos=ppos

	@property
	def pidx(self):return self._pidx
	@property
	def ppos(self):return self._ppos

	def __str__(self):
		return f"Spot({self._pidx},{self._ppos})"

	def __iter__(self):
		return iter((self._pidx,self._ppos))

	def __repr__(self):
		return self.__str__()


class Location:
	@typechecked
	def __init__(self,row:Row,col:Col):
		assert row>=0,"Location: row must be non negative"
		assert col>=0,"Location: col must be non negative"
		self._row=row
		self._col=col

	@property
	def row(self):return self._row
	@property
	def col(self):return self._col

	def __str__(self):
		return f"Location({self._row},{self._col})"

	def __iter__(self):
		return iter((self._row,self._col))

	def __repr__(self):
		return self.__str__()

# ██████╗ ██╗███████╗ ██████╗███████╗
# ██╔══██╗██║██╔════╝██╔════╝██╔════╝
# ██████╔╝██║█████╗  ██║     █████╗
# ██╔═══╝ ██║██╔══╝  ██║     ██╔══╝
# ██║     ██║███████╗╚██████╗███████╗
# ╚═╝     ╚═╝╚══════╝ ╚═════╝╚══════╝

class Piece:
	@typechecked
	def __init__(self,fpos:FPos,flen:FLen,src:Source,nlc:Nlc):
		self._fpos=fpos
		self._flen=flen
		self._src=src
		self._nlc=nlc

	@property
	def fpos(self):return self._fpos
	@property
	def flen(self):return self._flen
	@property
	def src(self):return self._src
	@property
	def nlc(self):return self._nlc

	def __str__(self):
		return f"Piece({self._fpos},{self._flen},{self._src},{self._nlc})"

	def __repr__(self):
		return self.__str__()

# piece
#  fpos
#  flen
#  src
#  nlc
# active piece
#  next
#  prev
# passive piece
#  pidx

# class ActivePiece(Piece):
# 	@typechecked
# 	def __init__(self,piece:Piece):
# 	# def __init__(self,fpos:FPos,flen:FLen,src:Source,nlc:Nlc):
# 		super(ActivePiece, self).__init__(piece.fpos,piece.flen,piece.src,piece.nlc)
# 		# super(ActivePiece, self).__init__(fpos,flen,src,nlc)
# 		self._next=None
# 		self._prev=None

# 	@property
# 	def next(self):return self._next
# 	@next.setter
# 	@typechecked
# 	def next(self,val:"ActivePiece"):self._next=val
# 	@next.deleter
# 	def next(self):self._next=None

# 	@property
# 	def prev(self):return self._prev
# 	@prev.setter
# 	@typechecked
# 	def prev(self,val:"ActivePiece"):self._prev=val
# 	@prev.deleter
# 	def prev(self):self._prev=None

# 	def passivate(self):
# 		return PassivePiece(super(ActivePiece,self))

# class PassivePiece(Piece):
# 	@typechecked
# 	def __init__(self,piece:Piece,pidx:PIdx):
# 	# def __init__(self,fpos:FPos,flen:FLen,src:Source,nlc:Nlc,pidx:PIdx):
# 		super(PassivePiece, self).__init__(piece.fpos,piece.flen,piece.src,piece.nlc)
# 		# super(PassivePiece, self).__init__(fpos,flen,src,nlc)
# 		self._pidx=pidx

# 	@property
# 	def pidx(self):return self._pidx

# 	def activate(self):
# 		return ActivePiece(super(PassivePiece,self))



#  █████╗  ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗████████╗ █████╗  ██████╗██╗  ██╗
# ██╔══██╗██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██╔════╝██║ ██╔╝
# ███████║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗   ██║   ███████║██║     █████╔╝
# ██╔══██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██╔═██╗
# ██║  ██║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║   ██║   ██║  ██║╚██████╗██║  ██╗
# ╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝

class ActionStack:
	class GroupState(Enum):
		Inactive=auto()
		First=auto()
		Active=auto()
		# Last=auto()
		def advance(self):
			return ActionStack.GroupState.Active if self==ActionStack.GroupState.First else self


	# class Substack:
	# 	def __init__(self):
	# 		pass

	class Action:

		def __init__(self,redo,undo,group_state,atomar_group_state):
			self.redo=redo
			self.undo=undo
			self.next=None
			self.prev=None
			self.group_state=group_state
			self.atomar_group_state=atomar_group_state


		def __str__(self):
			return f"Redo: {self.redo}, Undo: {self.undo}"

		def __repr__(self):
			return self.__str__()

	def __init__(self):
		self._next_atomar_group_state=ActionStack.GroupState.Inactive
		self._next_group_state=ActionStack.GroupState.Inactive
		self.curr=self._new_action()
		self.top=self.curr
		self.redos_possible=0
		self.undos_possible=0
		self.msg=lambda txt:None


	def _new_action(self,redo=None,undo=None):
		return ActionStack.Action(redo,undo,self._next_atomar_group_state,self._next_group_state)

	def act(self,redo,undo):

		# if self._atomar_group_is_enabled and self._new_atomar_group:
		# 	ags=GroupState.First

		new_act=self._new_action(redo,undo)

		self._next_atomar_group_state=self._next_atomar_group_state.advance()
		self._next_group_state=self._next_group_state.advance()

		new_act.prev=self.curr
		# Release memory here if next!=null.
		self.curr.next=new_act
		self.redos_possible=1
		self.redo()
		self.top=self.curr

	def __enter__(self):
		self._next_atomar_group_state=ActionStack.GroupState.First
	def __exit__(self,*args):
		self._next_atomar_group_state=ActionStack.GroupState.Inactive


	# a a a a a a a a a a a
	# f a a f a a i f a f f
	# f a a a a a a i i i i

	# redo: 1 und dann solange wie active
	# undo: solange bis inlc first


	def enable_group(self):
		self._next_group_state=ActionStack.GroupState.First
	def disable_group(self):
		self._next_group_state=ActionStack.GroupState.Inactive


	def undo(self):
		if self.curr.group_state==ActionStack.GroupState.Inactive:
			get_gs=lambda act:act.atomar_group_state
		else:
			get_gs=lambda act:act.group_state


		if get_gs(self.curr)==ActionStack.GroupState.Inactive:
			self._undo()
		else:
			while get_gs(self.curr)==ActionStack.GroupState.Active:
				self._undo()
			self._undo()


	def _undo(self):
		if self.curr.prev is None:
			self.msg("ActionStack._undo: Nothing to undo!")
		else:
			self.curr.undo()
			self.curr=self.curr.prev

			self.redos_possible+=1
			self.undos_possible-=1

	def redo(self):
		if self.curr.next is None:
			self.msg("ActionStack.redo: Nothing to redo!")
			return

		if self.curr.next.group_state==ActionStack.GroupState.Inactive:
			get_gs=lambda act:act.atomar_group_state
		else:
			get_gs=lambda act:act.group_state

		self._redo()
		while self.curr.next is not None and get_gs(self.curr.next)==ActionStack.GroupState.Active:
			self._redo()

	def _redo(self):
		if self.curr.next is None:
			self.msg("ActionStack._redo: Nothing to redo!")
		else:
			# self._redo()
			self.curr=self.curr.next
			self.curr.redo()

			self.redos_possible-=1
			self.undos_possible+=1

	# def _redo(self):

	# def _undo(self):

	def _iterate(self):
		curr_act=self.top
		index=0
		while curr_act.prev is not None:
			act=curr_act
			curr_act=curr_act.prev
			yield act,index==self.redos_possible
			index+=1

	def __str__(self):
		return "\n".join((" -> " if is_current else "    ")+str(elem) for elem,is_current in self._iterate())


	def __repr__(self):
		return self.__str__()

main()




