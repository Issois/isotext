
from enum import Enum


from buffer import Buffer

class Ui:
	def __init__(self):
		self.queue=[]
		self.pt=PieceTable("0123456789")
		self.msgq=[]
		self.pt.msg=lambda msg:self.msgq.append(msg)
		# self.pt.msg=print
		self.pt.actstack.msg=lambda msg:self.msgq.append(msg)

		self.act={
			Ui.Cmd.none:lambda _:None,
			Ui.Cmd.insert:lambda pt:self.insert(pt),
			Ui.Cmd.remove:lambda pt:self.remove(pt),
			Ui.Cmd.exit:lambda _:None,
			Ui.Cmd.undo:lambda pt:pt.undo(),
			Ui.Cmd.redo:lambda pt:pt.redo(),
		}

	def start(self):
		cmd=Ui.Cmd.none
		# print("! "+Ui.Cmd.all())
		while not cmd==Ui.Cmd.exit:
			# cmd=Ui.Cmd(self.get_input("? what to do: "))
			os.system("cls")
			print(
				str(self.pt)
				+"\n\n"
				+self.pt.pieces_str()
				+"\n\nOLD: "
				+self.pt.file[Source.Old]
				+"\nNEW: "
				+self.pt.file[Source.New]
				+"\n\n"
				+str(self.pt.actstack)
				+"\n"
			)
			if len(self.msgq)>0:
				errs="\n - ".join(self.msgq)
				print(f" - {errs}\n")
				self.msgq=[]
			cmd=Ui.Cmd(self.get_input(""))
			# print("! "+str(cmd))
			# return
			self.act[cmd](self.pt)


	def insert(self,pt):
		index=int(self.get_input("? index: "))
		content=self.get_input("? content: ").replace("\\n","\n")
		self.pt.insert(content,index)

	def remove(self,pt):
		start=int(self.get_input("? index start: "))
		stop=int(self.get_input("? index stop: "))
		self.pt.remove(start,stop)


	class Cmd(Enum):
		none=""
		insert="i"
		remove="d"
		exit="x"
		undo="u"
		redo="r"
		# substring="s"
		# pieces="p"

		def all():
		# def __str__():
			res=[]
			for data in Ui.Cmd:
				if not data.name=="none":
					res.append(f"{data.name}: {data.value}")
				# res.append(f"{data}")
			return ", ".join(res)


	def get_input(self,msg):
		if len(self.queue)==0:
			inp=input(msg)
			self.queue=shlex.split(inp)
			if len(self.queue)==0:
				self.queue=[""]

		return self.queue.pop(0)


def substrings(text):
	for count in range(1,len(text)+1):
		combs=combinations(text,count)
		for comb in combs:
			yield "".join(comb)

def main():

	buf=Buffer("hi\nthis\nis\ntext")
	print(f"<{nlc(buf)}>")


	return
	# ui=Ui()
	# ui.start()
main()