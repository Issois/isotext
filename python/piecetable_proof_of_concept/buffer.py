
from enum import Enum



class Action:
	def __init__(self):
		pass

	class Stack:
		def __init__(self):
			pass

class Target:
	def __init__(self):
		pass

	# class Po(Target):
	# 	def __init__(self):
	# 		pass
	# class Rc(Target):
	# 	def __init__(self):
	# 		pass


class Buffer:

	class Source(Enum):
		Old=0
		New=1

	def __init__(self,initial_text=None):

		self.actstack=Action.Stack()

		if initial_text is None:
			initial_text=""

		self.file={
			Buffer.Source.Old:initial_text,
			Buffer.Source.New:""
		}

		self.length=len(initial_text)
		self.nlc=Buffer._get_nlc(initial_text)

		# if self.length>0:
		# 	self.pt_head=Piece(
		# 		fpos=FPos(0),
		# 		flen=Flen(self.length),
		# 		src=Source.Old,
		# 		nlc=self.nlc
		# 	)
		# 	self.pt_len=1
		# else:
		# 	self.pt_head=None
		# 	self.pt_len=0

	def _get_nlc(text):return text.count("\n")
	def __len__(self):return self.length
	def __nlc__(self):return self.nlc

	def insert(target,content):pass
	def delete(target):pass
	def substr(target):pass
	def group_start():pass
	def group_end():pass
	def undo():pass
	def redo():pass
	def load():pass
	def save():pass

