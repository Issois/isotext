#ifndef INCLUDE_GUARD__MACRO_STR_H
#define INCLUDE_GUARD__MACRO_STR_H
#pragma message("--- INCLUDED   H: macro_str.h ")

#include <string>

#define _STR std::string
#define _STR_EMPTY ""
#define _STR_END std::string::npos


#endif /* INCLUDE_GUARD__MACRO_STR_H */