#ifndef INCLUDE_GUARD__BUFFER_H
#define INCLUDE_GUARD__BUFFER_H
#pragma message("--- INCLUDED   H: buffer.h ")


#include "macro_str.h"
// #include <string>
// #include <stdlib.h>
// <cstdint32_t>

class Buffer{
public:
	Buffer();
	Buffer(_STR inital_content);
	~Buffer();

	virtual size_t Length()const=0;
	virtual _STR Substr(size_t start, size_t length)const=0;
	virtual void Delete(size_t start, size_t length)=0;
	virtual void Insert(_STR text, size_t position)=0;

	_STR Substr(size_t start)const;
	_STR Substr()const;
	void Delete(size_t start);
	void Delete();
};




#endif /* INCLUDE_GUARD__BUFFER_H */