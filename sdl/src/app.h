#ifndef INCLUDE_GUARD__APP_H
#define INCLUDE_GUARD__APP_H
#pragma message("--- INCLUDED   H: app.h ")


#include <SDL.h>

#include "macro.h"
#include "using.h"


class App{
private:
	Using<int32_t>*           sdl;
	Using<int32_t>*           sdl_img;
	Using<int32_t>*           sdl_ttf;
	Using<SDL_Window*>*   sdl_window;
	Using<SDL_Renderer*>* sdl_renderer;

	void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int32_t x, int32_t y, int32_t w, int32_t h);
	SDL_Texture* renderText(const _STR &message, const _STR &fontFile,SDL_Color color, int32_t fontSize, SDL_Renderer *renderer);
	void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int32_t x, int32_t y);
	Using<int32_t>* Init_SDL();
	Using<int32_t>* Init_SDL_IMG();
	Using<int32_t>* Init_SDL_TTF();
	Using<SDL_Window*>* Init_SDL_Window();
	Using<SDL_Renderer*>* Init_SDL_Renderer();

public:
	App();
	~App();

	void Loop();
	bool Success();

};



#endif /* INCLUDE_GUARD__APP_H */