#ifndef INCLUDE_GUARD__BUFFER_STRING_H
#define INCLUDE_GUARD__BUFFER_STRING_H
#pragma message("--- INCLUDED   H: buffer_string.h ")


#include "macro_str.h"

#include "buffer.h"

class StringBuffer:public Buffer{
private:
	_STR content;
public:
	StringBuffer();
	StringBuffer(_STR inital_content);
	~StringBuffer();
	size_t Length()const;
	_STR Substr(size_t start, size_t length)const;
	void Delete(size_t start, size_t length);
	void Insert(_STR text, size_t position);
};




#endif /* INCLUDE_GUARD__BUFFER_STRING_H */