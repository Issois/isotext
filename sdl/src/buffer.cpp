#include "macro_str.h"
#include "macro_io.h"
#include "buffer.h"

Buffer::Buffer(){
	_PR("Buffer() called.");
}
Buffer::Buffer(_STR inital_content){
	_PR("Buffer(_STR) called.");
}
Buffer::~Buffer(){
	_PR("~Buffer() called.");
}

_STR Buffer::Substr(size_t start)const{
	return this->Substr(start,_STR_END);
}
_STR Buffer::Substr()const{
	return this->Substr(0);
}
void Buffer::Delete(size_t start){
	return this->Delete(start,_STR_END);
}
void Buffer::Delete(){
	return this->Delete(0);
}

