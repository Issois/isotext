#ifndef INCLUDE_GUARD__ROPE_NODE_H
#define INCLUDE_GUARD__ROPE_NODE_H
#pragma message("--- INCLUDED   H: rope_node.h ")

#include "macro_ptr.h"
#include "macro_str.h"
// #include "macro_opt.h"
#include "macro_dict.h"

class RopeNode{
public:
	enum Side{Left,Right};
	RopeNode(_STR content);
	~RopeNode();
	// bool IsLeaf();
	// bool IsRoot();
	// Add(RopeNode child, Side side);
	*RopeNode Replace(*RopeNode child, Side side);
	*RopeNode Remove(Side side);
	*RopeNode Child(Side side);
private:
	update();
	_DICT<Side,*RopeNode> children;
	// RopeNode test;
	*RopeNode parent;
	size_t weight;
	const _STR content;
	// Distance To Leaf
	size_t dtl_min;
	size_t dtl_max;


};

#endif /* INCLUDE_GUARD__ROPE_NODE_H */