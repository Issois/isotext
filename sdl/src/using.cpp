#ifndef INCLUDE_GUARD__USING_CPP
#define INCLUDE_GUARD__USING_CPP
#pragma message("--- INCLUDED   CPP: using.cpp ")

#include "using.h"


#include "macro.h"

template <typename T>
Using<T>::Using(){}

template <typename T>
Using<T>::Using(std::function<T()> generator, std::function<bool(T)> success, std::function<void(T)> destructor, std::function<void()> onError){
	this->success=success;
	this->destructor=destructor;
	if(_EX(generator)){
		this->obj=generator();
	}
	if(!this->Success() && _EX(onError)){
		onError();
	}
}

template <typename T>
Using<T>::~Using(){
	if(_EX(this->destructor)){
		this->destructor(this->obj);
	}
}

template <typename T>
T Using<T>::Obj(){
	return this->obj;
}
template <typename T>
bool Using<T>::Success(){
	return _EX(this->success)?this->success(this->obj):false;
}



#endif /* INCLUDE_GUARD__USING_CPP */