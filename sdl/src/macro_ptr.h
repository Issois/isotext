#ifndef INCLUDE_GUARD__MACRO_PTR_H
#define INCLUDE_GUARD__MACRO_PTR_H
#pragma message("--- INCLUDED   H: macro_ptr.h ")

#include <memory>

#define _PTR std::unique_ptr

#endif /* INCLUDE_GUARD__MACRO_PTR_H */