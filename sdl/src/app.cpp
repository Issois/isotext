#ifndef INCLUDE_GUARD__APP_CPP
#define INCLUDE_GUARD__APP_CPP
#pragma message("--- INCLUDED   CPP: app.cpp ")

#include "app.h"

#include <vector>
#include <fstream>
#include <string>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "macro.h"
#include "using.h"


// Function initializations.
void ReadFile(std::vector<_STR>* vec, _STR path);


App::App(){
	if(!(this->sdl         =this->Init_SDL()         )->Success()) return;
	if(!(this->sdl_img     =this->Init_SDL_IMG()     )->Success()) return;
	if(!(this->sdl_ttf     =this->Init_SDL_TTF()     )->Success()) return;
	if(!(this->sdl_window  =this->Init_SDL_Window()  )->Success()) return;
	if(!(this->sdl_renderer=this->Init_SDL_Renderer())->Success()) return;
}

App::~App(){
	if(this->sdl->Success())         delete this->sdl;
	if(this->sdl_img->Success())     delete this->sdl_img;
	if(this->sdl_ttf->Success())     delete this->sdl_ttf;
	if(this->sdl_window->Success())  delete this->sdl_window;
	if(this->sdl_renderer->Success())delete this->sdl_renderer;
}
bool App::Success(){
	return this->sdl->Success()
		&& this->sdl_img->Success()
		&& this->sdl_ttf->Success()
		&& this->sdl_window->Success()
		&& this->sdl_renderer->Success();
}

void App::Loop(){
	if(!this->Success()) return;
	SDL_Color color = {255,255,255,255};

	// _STR imagePath="crazy.png";
	// SDL_Texture *texture=IMG_LoadTexture(this->sdl_renderer->Obj(),imagePath.c_str());
	// SDL_Texture *text;
	// TTF_Font *font = TTF_OpenFont("consola.ttf",10);
	// if (_NEX(font)){
	// 	_PR("TTF_OpenFont error");
	// 	return;
	// }
	// string message="The quick brown fox jumps over the lazy dog";
	// //We need to first render to a surface as that's what TTF_RenderText
	// //returns, then load that surface int32_to a texture
	// SDL_Surface *surf = TTF_RenderUTF8_Solid(font, message.c_str(), color);
	// if(_EX(surf)){
	// 	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surf);
	// 	if(_EX(texture)){
	// 		int32_t w, h;
	// 		SDL_QueryTexture(tex, NULL, NULL, &w, &h);
	// 		renderTexture(tex, ren, x, y, w, h);

	// 		SDL_Rect dst;
	// 		dst.x = x;
	// 		dst.y = y;
	// 		dst.w = w;
	// 		dst.h = h;
	// 		SDL_RenderCopy(ren, tex, NULL, &dst);
	// 	}
	// }
	// //Clean up the surface and font
	// SDL_FreeSurface(surf);

	std::vector<_STR> vec;
	ReadFile(&vec,"..\\res\\test.txt");
	// for(int32_t i=0;i<vec.size();i++){
	// 	_PR(vec[i]);
	// }
	// return;

	// TTF_CloseFont(font);
	SDL_Texture* text;

	int32_t size=20;
	text=renderText("x","..\\res\\consola.ttf",color,size,this->sdl_renderer->Obj());
	int32_t width_char, height_char;
	SDL_QueryTexture(text,NULL,NULL,&width_char,&height_char);


	// return
	//e is an SDL_Event variable we've declared before entering the main loop
	auto quit=false;
	// int32_t pos=0;
	int32_t x=0;
	// int32_t i=0;
	SDL_Event event;
	int32_t width_window;
	int32_t height_window;
	int32_t pos_row=0;
	int32_t pos_col=0;
	int32_t cnt_row;
	int32_t cnt_col;
	// int32_t col_len;
	_STR line;
	while(!quit){
		x++;
		SDL_WaitEvent(&event);
		// _PR("Event: "<<x<<" "<<event.type);

		if (event.type == SDL_QUIT){
			quit = true;
		}
		if (event.type == SDL_KEYDOWN){

			// i++;
		}
		// If user clicks the mouse
		if (event.type == SDL_MOUSEBUTTONDOWN){
			pos_row=(pos_row+1)%10;
		// 	if(_EX(texture)){
		// 		renderTexture(texture,this->sdl_renderer->Obj(),pos,pos);
		// 		SDL_RenderPresent(this->sdl_renderer->Obj());
		// 		// SDL_Delay(2000);
		// 		pos+=10;
		// 	}else{
		// 		_PR("IMG_LoadTexture Error: "<<SDL_GetError());
		// 	}
		}
		// }

		SDL_RenderClear(this->sdl_renderer->Obj());
		SDL_GetWindowSize(this->sdl_window->Obj(),&width_window,&height_window);
		cnt_row=(height_window/height_char)+1;
		cnt_col=(width_window/width_char)+1;
		// while()
		for(int32_t row=pos_row;row<pos_row+cnt_row && row<vec.size();row++){
			if(pos_col<vec[row].length()){
				text=renderText(vec[row].substr(pos_col,cnt_col),"..\\res\\consola.ttf",color,size,this->sdl_renderer->Obj());
				renderTexture(text,this->sdl_renderer->Obj(),0,(row-pos_row)*height_char);
			}
		}

		SDL_RenderPresent(this->sdl_renderer->Obj());

	}

	// _STR imagePath="test.bmp";
	// SDL_Surface* bmp=SDL_LoadBMP(imagePath.c_str());
	// if(_EX(bmp)){
	// 	SDL_Texture* tex = SDL_CreateTextureFromSurface(this->sdl_renderer->Obj(),bmp);
	// 	SDL_FreeSurface(bmp);
	// 	if(_EX(tex)){
	// 		for (int32_t i = 0; i < 3; ++i){
	// 			//First clear the renderer
	// 			SDL_RenderClear(this->sdl_renderer->Obj());
	// 			//Draw the texture
	// 			SDL_RenderCopy(this->sdl_renderer->Obj(), tex, NULL, NULL);
	// 			//Update the screen
	// 			SDL_RenderPresent(this->sdl_renderer->Obj());
	// 			//Take a quick break after all that hard work
	// 			SDL_Delay(300);
	// 		}
	// 	}
	// }else{
	// 	_PR("Image not found.");
	// }
}


void ReadFile(std::vector<_STR>* vec, _STR path){
	_STR line;

	std::ifstream myfile;
	myfile.open(path);

	if(!myfile.is_open()) {
		_PR("Error reading file");
		return;
	}

	while(std::getline(myfile,line)){
		vec->push_back(line);
	}
}


void App::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int32_t x, int32_t y, int32_t w, int32_t h){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	dst.w = w;
	dst.h = h;
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

SDL_Texture* App::renderText(const _STR &message, const _STR &fontFile,SDL_Color color, int32_t fontSize, SDL_Renderer *renderer){
	//Open the font
	TTF_Font *font = TTF_OpenFont(fontFile.c_str(), fontSize);
	if (font == nullptr){
		_PR("TTF_OpenFont error");
		return nullptr;
	}
	//We need to first render to a surface as that's what TTF_RenderText
	//returns, then load that surface int32_to a texture
	// SDL_Color bg={0,0,255,255};
	// SDL_Surface *surf=TTF_RenderUTF8_LCD(font, message.c_str(), color,bg);
	SDL_Surface *surf=TTF_RenderUTF8_Blended(font, message.c_str(), color);

	if (surf == nullptr){
		TTF_CloseFont(font);
		_PR("TTF_RenderText error");
		return nullptr;
	}
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surf);
	if (texture == nullptr){
		_PR("CreateTexture error");
	}
	//Clean up the surface and font
	SDL_FreeSurface(surf);
	TTF_CloseFont(font);
	return texture;
}

void App::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int32_t x, int32_t y){
	int32_t w, h;
	SDL_QueryTexture(tex, NULL, NULL, &w, &h);
	renderTexture(tex, ren, x, y, w, h);
}






Using<int32_t>* App::Init_SDL(){
	return new Using<int32_t>(
		[](){return SDL_Init(SDL_INIT_VIDEO);},
		[](int32_t result){return result==0;},
		[](int32_t){SDL_Quit();_PR("SDL_Quit");},
		[](){_PR("SDL_Init Error: "<<SDL_GetError());}
	);
}

Using<int32_t>* App::Init_SDL_IMG(){
	return new Using<int32_t>(
		[](){return IMG_Init(IMG_INIT_PNG);},
		[](int32_t result){return (result & IMG_INIT_PNG) == IMG_INIT_PNG;},
		[](int32_t){IMG_Quit();_PR("IMG_Quit");},
		[](){_PR("IMG_Quit Error: "<<SDL_GetError());}
	);
}

Using<int32_t>* App::Init_SDL_TTF(){
	return new Using<int32_t>(
		[](){return TTF_Init();},
		[](int32_t result){return result==0;},
		[](int32_t){TTF_Quit();_PR("TTF_Quit");},
		[](){_PR("TTF_Quit Error: "<<SDL_GetError());}
	);
}

Using<SDL_Window*>* App::Init_SDL_Window(){
	return new Using<SDL_Window*>(
		[](){return SDL_CreateWindow("Hello World!",100,100,640,480,SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);},
		[](SDL_Window* window){return _EX(window);},
		[](SDL_Window* window){SDL_DestroyWindow(window);_PR("SDL_DestroyWindow");},
		[](){_PR("SDL_CreateWindow Error: "<<SDL_GetError());}
	);
}

Using<SDL_Renderer*>* App::Init_SDL_Renderer(){
	auto window=this->sdl_window;
	return new Using<SDL_Renderer*>(
		[window](){return SDL_CreateRenderer(window->Obj(),-1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);},
		[](SDL_Renderer* ren){return _EX(ren);},
		[](SDL_Renderer* ren){SDL_DestroyRenderer(ren);_PR("SDL_DestroyRenderer");},
		[](){_PR("SDL_CreateRenderer Error: "<<SDL_GetError());}
	);
}



#endif /* INCLUDE_GUARD__APP_CPP */