#ifndef INCLUDE_GUARD__PIECE_TABLE_H
#define INCLUDE_GUARD__PIECE_TABLE_H
#pragma message("--- INCLUDED   H: piece_table.h ")


#include <list>

#include "macro_str.h"
#include "buffer.h"

const struct Piece{
	size_t Start;
	size_t Length;
	int NewlineCount;
};

class PieceTable:public Buffer{
private:
	_STR buffer;
	size_t length;
	size_t length_initial;
	std::list<Piece> pieces;

public:
	PieceTable(_STR content);
	~PieceTable();
	size_t Length()const;
	_STR Substr(size_t start, size_t length)const;
	void Delete(size_t start, size_t length);
	void Insert(_STR text, size_t position);
};

#endif /* INCLUDE_GUARD__PIECE_TABLE_H */