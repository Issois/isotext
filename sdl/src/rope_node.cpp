#ifndef INCLUDE_GUARD__ROPE_NODE_CPP
#define INCLUDE_GUARD__ROPE_NODE_CPP
#pragma message("--- INCLUDED   CPP: rope_node.cpp ")

#include "macro_io.h"
#include "macro_str.h"
// #include "macro_opt.h"
#include "rope_node.h"

RopeNode::RopeNode(_STR content):content(content){
	this->dtl_min=0;
	this->dtl_max=0;
	this->children[Left]=nullptr;
	this->children[Right]=nullptr;
}

RopeNode::~RopeNode(){
	_PR("DECONSTR: "<<this->content);
	// this->parent=nullptr;
	delete this->children[Left];
	delete this->children[Right];
}

// bool RopeNode::IsLeaf(){
// 	return _EX(this->left)||_EX(this->right);
// }

// bool RopeNode::IsRoot(){
// 	return _NEX(this->parent);
// }

// RopeNode::Add(RopeNode child, Side side){
// 	// if not exists.
// 	this.Replace(child,side);
// }
RopeNode::update(){
	RopeNode* cl=this->children[Left];
	RopeNode* cr=this->children[Right];
	if(cl==nullptr){
		if(cr==nullptr){
			this->dtl_max=-1;
			this->dtl_min=-1;
		}else{
			this->dtl_max=cr->dtl_max;
			this->dtl_min=cr->dtl_min;
		}
	}else{
		if(cr==nullptr){
			this->dtl_max=cl->dtl_max;
			this->dtl_min=cl->dtl_min;
		}else{
			this->dtl_max= cl->dtl_max>cr->dtl_max ? cl->dtl_max : cr->dtl_max;
			this->dtl_min= cl->dtl_min<cr->dtl_min ? cl->dtl_min : cr->dtl_min;
		}
	}
	this->dtl_max++;
	this->dtl_min++;
	if(this->parent!=nullptr){
		this->parent.update();
	}
}

RopeNode* RopeNode::Replace(RopeNode* child, Side side){
	RopeNode* replaced=this->Remove(side);
	this->children[side]=child;
	return replaced;
}

RopeNode* RopeNode::Remove(Side side){
	RopeNode* removed=this->children[side];
	this->children[side]=nullptr;
	return removed;
}

*RopeNode RopeNode::Child(Side side){
	return this.children[side];
}




#endif /* INCLUDE_GUARD__ROPE_NODE_CPP */