#ifndef INCLUDE_GUARD__USING_H
#define INCLUDE_GUARD__USING_H
#pragma message("--- INCLUDED   H: using.h ")


#include <functional>

template <typename T>
class Using{
private:
	std::function<void(T)> destructor;
	std::function<bool(T)> success;
	T obj;
public:
	Using();
	Using(std::function<T()> generator, std::function<bool(T)> success, std::function<void(T)> destructor, std::function<void()> onError);
	~Using();
	T Obj();
	bool Success();
};




#endif /* INCLUDE_GUARD__USING_H */