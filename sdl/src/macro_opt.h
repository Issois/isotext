#ifndef INCLUDE_GUARD__MACRO_OPT_H
#define INCLUDE_GUARD__MACRO_OPT_H
#pragma message("--- INCLUDED   H: macro_opt.h ")

#include <optional>

#define _OPT std::optional
#define _OPT_NONE std::nullopt


#endif /* INCLUDE_GUARD__MACRO_OPT_H */