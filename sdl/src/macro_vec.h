#ifndef INCLUDE_GUARD__MACRO_VEC_H
#define INCLUDE_GUARD__MACRO_VEC_H
#pragma message("--- INCLUDED   H: macro_vec.h ")

#include <vector>

#define _VEC std::vector

#endif /* INCLUDE_GUARD__MACRO_VEC_H */