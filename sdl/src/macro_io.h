#ifndef INCLUDE_GUARD__MACRO_IO_H
#define INCLUDE_GUARD__MACRO_IO_H
#pragma message("--- INCLUDED   H: macro_io.h ")


#include <iostream>

#define _PR(str) std::cout<<str<<std::endl


#endif /* INCLUDE_GUARD__MACRO_IO_H */