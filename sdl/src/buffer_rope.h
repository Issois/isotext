#ifndef INCLUDE_GUARD__BUFFER_ROPE_H
#define INCLUDE_GUARD__BUFFER_ROPE_H
#pragma message("--- INCLUDED   H: buffer_rope.h ")


#include "macro.h"

#include "buffer.h"

class RopeBuffer:public Buffer{
private:
	_STR content;
public:
	RopeBuffer();
	RopeBuffer(_STR inital_content);
	~RopeBuffer();
	size_t Length()const;
	_STR Substr(size_t start, size_t length)const;
	void Delete(size_t start, size_t length);
	void Insert(_STR text, size_t position);
};





#endif /* INCLUDE_GUARD__BUFFER_ROPE_H */