#ifndef INCLUDE_GUARD__ROPE_H
#define INCLUDE_GUARD__ROPE_H
#pragma message("--- INCLUDED   H: rope.h ")


#include "macro.h"
#include "rope_node.h"

class Rope{
private:
	RopeNode* root;
public:
	Rope(_VEC<RopeNode> nodes);
	~Rope();

};



#endif /* INCLUDE_GUARD__ROPE_H */