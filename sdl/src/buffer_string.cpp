
#include "macro.h"
#include "buffer_string.h"

StringBuffer::StringBuffer(){
	this->content=_STR_EMPTY;
}
StringBuffer::StringBuffer(_STR inital_content){
	this->content=inital_content;
}
StringBuffer::~StringBuffer(){}

size_t StringBuffer::Length()const{
	// this->content=_STR_EMPTY;
	return this->content.length();
}
_STR StringBuffer::Substr(size_t start, size_t length)const{
	_STR result;
	if(start>=this->content.length()){
		result=_STR_EMPTY;
	}else{
		result=this->content.substr(start,length);
	}
	return result;
}
void StringBuffer::Delete(size_t start, size_t length){
	if(start<this->content.length()){
		this->content.erase(start,length);
	}
}
void StringBuffer::Insert(_STR text, size_t _position){
	size_t len=this->content.length();
	size_t position=_position>len?len:_position;
	this->content.insert(position,text);
}

