#ifndef INCLUDE_GUARD__BUFFER_ROPE_CPP
#define INCLUDE_GUARD__BUFFER_ROPE_CPP
#pragma message("--- INCLUDED   CPP: buffer_rope.cpp ")


#include "macro.h"
#include "buffer_rope.h"

RopeBuffer::RopeBuffer(){
	this->content=_STR_EMPTY;
}
RopeBuffer::RopeBuffer(_STR inital_content){
	this->content=inital_content;
}
RopeBuffer::~RopeBuffer(){}

size_t RopeBuffer::Length()const{
	// this->content=_STR_EMPTY;
	return this->content.length();
}
_STR RopeBuffer::Substr(size_t start, size_t length)const{
	_STR result;
	if(start>=this->content.length()){
		result=_STR_EMPTY;
	}else{
		result=this->content.substr(start,length);
	}
	return result;
}
void RopeBuffer::Delete(size_t start, size_t length){
	if(start<this->content.length()){
		this->content.erase(start,length);
	}
}
void RopeBuffer::Insert(_STR text, size_t position){}



#endif /* INCLUDE_GUARD__BUFFER_ROPE_CPP */