
#include <chrono>
#include <SDL.h>

#include "macro_str.h"
#include "macro_io.h"
#include "piece_table.h"
// #include "app.h"
// #include "buffer_string.h"
// #include "buffer_rope.h"
// #include "rope_node.h"

// Function initializations.
// void Test_Buffer_internal1(Buffer* buf);
// void Test_Buffer();
// void donothing(_STR s);
// __int64 time(std::function<void()> fun);


int32_t main(int32_t,char**){
	// _set_error_mode(_OUT_TO_STDERR);
	_CrtSetReportMode(_CRT_ERROR,_CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ERROR,_CRTDBG_FILE_STDERR);

	// RopeNode n1=RopeNode("n1");
	// {
	// 	// RopeNode n2=RopeNode("n2");
	// 	// n1.Replace(&n2,RopeNode::Left);
	// 	// RopeNode n2=;
	// 	n1.Replace(new RopeNode("n2"),RopeNode::Left);
	// }
	// _PR("LEFt inner");
	// PieceTable pt=PieceTable("Test");
	PieceTable pt("Test");
	Buffer* buf=&pt;
	_PR(buf->Substr());

	// Test_Buffer();
	// auto app=App();
	// app.Loop();
	return 0;
}





// void donothing(_STR s){}

// void Test_Buffer(){
// 	_STR text="fox jumps over stuff";
// 	StringBuffer sbuf=StringBuffer(text);
// 	RopeBuffer rbuf=RopeBuffer(text);

// 	_VEC<Buffer*> bufs{
// 		&sbuf,
// 		&rbuf
// 	};

// 	for(Buffer* buf:bufs){
// 		_PR(_STR_EMPTY);
// 		Test_Buffer_internal1(buf);
// 		// break;
// 	}
// 	_PR(_STR_EMPTY);
// }

// void Test_Buffer_internal1(Buffer* buf){
// 	for(int i=0;i<5;i++){
// 		_PR(time([buf](){
// 			// int y=0;
// 			for(int i=0;i<5e3;i++){
// 				// y++;
// 				// donothing("nice");
// 				buf->Insert("yikes",0);
// 			}
// 		})/1e6);
// 	}
// }

// __int64 time(std::function<void()> fun){
// 	auto start=std::chrono::high_resolution_clock::now();
// 	fun();
// 	auto end=std::chrono::high_resolution_clock::now();
// 	// std::chrono::duration<int,std::micro> duration=end-start;
// 	return (end-start).count();
// }


