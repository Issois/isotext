#ifndef INCLUDE_GUARD__MACRO_DICT_H
#define INCLUDE_GUARD__MACRO_DICT_H
#pragma message("--- INCLUDED   H: macro_dict.h ")

#include <unordered_map>

#define _DICT std::unordered_map

#endif /* INCLUDE_GUARD__MACRO_DICT_H */