#ifndef UNICODE
#define UNICODE
#endif

#include <windows.h>
#include <stdio.h>
#include <shobjidl.h>
// #include <atlbase.h> // Contains the declaration of CComPtr.

int FileDialog();
template <class T> void SafeRelease(T **ppT);
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow){

	// FileDialog();

	// Register the window class.
	const wchar_t CLASS_NAME[]=L"Sample Window Class";

	WNDCLASS wc={};

	wc.lpfnWndProc  =WindowProc;
	wc.hInstance    =hInstance;
	wc.lpszClassName=CLASS_NAME;

	RegisterClass(&wc);

	// Create the window.

	HWND hwnd = CreateWindow(
		// 0,                              // Optional window styles.
		CLASS_NAME,                     // Window class
		L"test",    // Window text
		WS_OVERLAPPEDWINDOW,            // Window style
		// WS_DLGFRAME|WS_BORDER,
		// WS_VISIBLE | WS_POPUP,
		// 0,
		// Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		// 500,500,200,200,

		NULL,       // Parent window
		NULL,       // Menu
		hInstance,  // Instance handle
		NULL        // Additional application data
		);

	// fwprintf(stdout,L"Test");
	if(hwnd==NULL){
		return 0;
	}

	// if (FAILED(CoInitializeEx(NULL,COINIT_APARTMENTTHREADED|COINIT_DISABLE_OLE1DDE))){
	// 	return 0;
	// }


	ShowWindow(hwnd,nCmdShow);

	// Run the message loop.

	MSG msg={};
	while(GetMessage(&msg,NULL,0,0)>0){
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}

int test=1;

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	switch(uMsg){
	case WM_DESTROY:
		// CoUninitialize();
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd, &ps);

			// All painting occurs here, between BeginPaint and EndPaint.

			FillRect(hdc, &ps.rcPaint, (HBRUSH) (COLOR_WINDOW+((++test)%10)));

			EndPaint(hwnd, &ps);
		}
		return 0;

	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}


int FileDialog(){
	HRESULT hr=CoInitializeEx(NULL,COINIT_APARTMENTTHREADED|COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr)){
		IFileOpenDialog *pFileOpen;
		// Create the FileOpenDialog object.
		// hr=CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileOpen));
		hr=CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_PPV_ARGS(&pFileOpen));

		if(SUCCEEDED(hr)){
			// Show the Open dialog box.
			hr = pFileOpen->Show(NULL);

			// Get the file name from the dialog box.
			if (SUCCEEDED(hr)){
				IShellItem *pItem;
				hr = pFileOpen->GetResult(&pItem);
				if (SUCCEEDED(hr)){
					PWSTR pszFilePath;
					hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

					// Display the file name to the user.
					if (SUCCEEDED(hr)){
						MessageBoxW(NULL, pszFilePath, L"File Path", MB_OK);
						CoTaskMemFree(pszFilePath);
					}
					SafeRelease(&pItem);
				}
			}
			SafeRelease(&pFileOpen);
		}
		CoUninitialize();
	}
	return 0;
}

template <class T> void SafeRelease(T **ppT){
	if (*ppT){
		(*ppT)->Release();
		*ppT = NULL;
	}
}
