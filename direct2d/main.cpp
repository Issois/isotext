#include <windows.h>
#include <d2d1.h>
#pragma comment(lib, "d2d1")
#include <dwrite.h>
#pragma comment(lib, "dwrite")
#include <iostream>

#include ".\basewindow.h"
#include ".\other.cpp"



template <class T> void SafeRelease(T **ppT)
{
	if (*ppT)
	{
		(*ppT)->Release();
		*ppT = NULL;
	}
}

class MainWindow : public BaseWindow<MainWindow>
{
	ID2D1Factory            *pD2DFactory_;
	ID2D1HwndRenderTarget   *pRT_;
	ID2D1SolidColorBrush    *pBlackBrush_;
	D2D1_ELLIPSE            ellipse;
	IDWriteFactory* pDWriteFactory_;
	IDWriteTextFormat* pTextFormat_;
	wchar_t* wszText_;
	std::wstring text;
	UINT32 cTextLength_;

	void    CalculateLayout();
	HRESULT CreateGraphicsResources();
	void    DiscardGraphicsResources();
	void    OnPaint();
	void    Resize();
	void    DrawText();

	public:

	MainWindow() : pD2DFactory_(NULL), pRT_(NULL), pBlackBrush_(NULL)
	{
	}

	PCWSTR  ClassName() const { return L"Circle Window Class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
};

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow)
{
	// test();
	// std::string text="sad";
	// PR(text.length());
	// return 0;
	MainWindow win;

	if (!win.Create(L"Circle", WS_OVERLAPPEDWINDOW))
	{
		return 0;
	}

	ShowWindow(win.Window(), nCmdShow);

	// Run the message loop.

	MSG msg = { };
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}

// Recalculate drawing layout when the size of the window changes.

void MainWindow::CalculateLayout()
{
	if (pRT_ != NULL)
	{
		D2D1_SIZE_F size = pRT_->GetSize();
		const float x = size.width / 2;
		const float y = size.height / 2;
		const float radius = min(x, y);
		ellipse = D2D1::Ellipse(D2D1::Point2F(x, y), radius, radius);
	}
}

HRESULT MainWindow::CreateGraphicsResources()
{
	HRESULT hr = S_OK;
	if (pRT_ == NULL)
	{
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

		hr = pD2DFactory_->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(m_hwnd, size),
			&pRT_);

		if (SUCCEEDED(hr))
		{
			const D2D1_COLOR_F color = D2D1::ColorF(1.0f, 1.0f, 1.0f);
			hr = pRT_->CreateSolidColorBrush(color, &pBlackBrush_);

			if (SUCCEEDED(hr))
			{
				CalculateLayout();
			}
		}
	}
	return hr;
}

void MainWindow::DiscardGraphicsResources()
{
	SafeRelease(&pRT_);
	SafeRelease(&pBlackBrush_);
}

void MainWindow::DrawText(){
	RECT rc;
	GetClientRect(m_hwnd, &rc);
	D2D1_RECT_F layoutRect = D2D1::RectF(
		static_cast<FLOAT>(rc.left) / 1,
		static_cast<FLOAT>(rc.top) / 1,
		static_cast<FLOAT>(rc.right - rc.left) / 1,
		static_cast<FLOAT>(rc.bottom - rc.top) / 1
		);

	cTextLength_ = (UINT32) this->text.length();
	// wszText_ = L"static text";
	// cTextLength_ = (UINT32) wcslen(wszText_);
	pDWriteFactory_->CreateTextFormat(
		L"Consolas",                // Font family name.
		NULL,                       // Font collection (NULL sets it to use the system font collection).
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		72.0f,
		L"en-us",
		&pTextFormat_
		);

	pRT_->DrawText(
		this->text.c_str(),        // The string to render.
		cTextLength_,    // The string's length.
		pTextFormat_,    // The text format.
		layoutRect,       // The region of the window where the text will be rendered.
		pBlackBrush_     // The brush used to draw the text.
		);
}

void MainWindow::OnPaint()
{
	HRESULT hr = CreateGraphicsResources();
	if (SUCCEEDED(hr))
	{
		PAINTSTRUCT ps;
		BeginPaint(m_hwnd, &ps);

		pRT_->BeginDraw();

		pRT_->Clear( D2D1::ColorF(D2D1::ColorF::Black) );
		// pRT_->FillEllipse(ellipse, pBlackBrush_);
		DrawText();

		hr = pRT_->EndDraw();
		if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET)
		{
			DiscardGraphicsResources();
		}
		EndPaint(m_hwnd, &ps);
	}
}

void MainWindow::Resize()
{
	if (pRT_ != NULL)
	{
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

		pRT_->Resize(size);
		CalculateLayout();
		InvalidateRect(m_hwnd, NULL, FALSE);
	}
}


LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	HRESULT hr;
	switch (uMsg)
	{
	case WM_CREATE:
		if (FAILED(D2D1CreateFactory(
				D2D1_FACTORY_TYPE_SINGLE_THREADED, &pD2DFactory_)))
		{
			return -1;  // Fail CreateWindowEx.
		}
		this->text=L"1";
		hr = DWriteCreateFactory(
			DWRITE_FACTORY_TYPE_SHARED,
			__uuidof(IDWriteFactory),
			reinterpret_cast<IUnknown**>(&pDWriteFactory_)
			);

		return 0;

	case WM_DESTROY:
		DiscardGraphicsResources();
		SafeRelease(&pD2DFactory_);
		PostQuitMessage(0);
		return 0;

	case WM_LBUTTONDOWN:
		this->text+=L"1";
		// wszText_+=L"1";
	case WM_PAINT:
		OnPaint();
		return 0;



	case WM_SIZE:
		Resize();
		return 0;
	}
	return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
}


// HRESULT DemoApp::DrawHelloWorld(ID2D1HwndRenderTarget* pIRenderTarget){
// 	HRESULT hr = S_OK;
// 	ID2D1SolidColorBrush* pIRedBrush = NULL;
// 	IDWriteTextFormat* pITextFormat = NULL;
// 	IDWriteFactory* pIDWriteFactory = NULL;

// 	if (SUCCEEDED(hr))
// 	{
// 		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,
// 				__uuidof(IDWriteFactory),
// 				reinterpret_cast<IUnknown**>(&pIDWriteFactory));
// 	}

// 	if(SUCCEEDED(hr))
// 	{
// 		hr = pIDWriteFactory->CreateTextFormat(
// 			L"Arial",
// 			NULL,
// 			DWRITE_FONT_WEIGHT_NORMAL,
// 			DWRITE_FONT_STYLE_NORMAL,
// 			DWRITE_FONT_STRETCH_NORMAL,
// 			10.0f * 96.0f/72.0f,
// 			L"en-US",
// 			&pITextFormat
// 		);
// 	}

// 	if(SUCCEEDED(hr))
// 	{
// 		hr = pIRenderTarget->CreateSolidColorBrush(
// 			D2D1:: ColorF(D2D1::ColorF::Red),
// 			&pIRedBrush
// 		);
// 	}

// 	D2D1_RECT_F layoutRect = D2D1::RectF(0.f, 0.f, 100.f, 100.f);

// 	// Actually draw the text at the origin.
// 	if(SUCCEEDED(hr))
// 	{
// 		pIRenderTarget->DrawText(
// 			L"Hello World",
// 			wcslen(L"Hello World"),
// 			pITextFormat,
// 			layoutRect,
// 			pIRedBrush
// 		);
// 	}

// 	// Clean up.
// 	SafeRelease(&pIRedBrush);
// 	SafeRelease(&pITextFormat);
// 	SafeRelease(&pIDWriteFactory);

// 	return hr;
// }