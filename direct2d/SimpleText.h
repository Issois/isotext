
#include <dwrite.h>

class SimpleText{
	IDWriteFactory* pDWriteFactory_;
	IDWriteTextFormat* pTextFormat_;

	const wchar_t* wszText_;
	UINT32 cTextLength_;

	ID2D1Factory* pD2DFactory_;
	ID2D1HwndRenderTarget* pRT_;
	ID2D1SolidColorBrush* pBlackBrush_;

}
