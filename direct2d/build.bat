@echo off
echo "  ____  _    _ _____ _      _____  "&
echo " |  _ \| |  | |_   _| |    |  __ \ "&
echo " | |_) | |  | | | | | |    | |  | |"&
echo " |  _ <| |  | | | | | |    | |  | |"&
echo " | |_) | |__| |_| |_| |____| |__| |"&
echo " |____/ \____/|_____|______|_____/ "&
echo "                                   "&
echo "                                   "
set csprojFile=project.vcxproj
set compilerName=MSBuild.exe
set compilerLocation="C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\MSBuild\Current\Bin\%compilerName%"
set baseDir=%~dp0
set binaryLocation=%baseDir%bin\
set binaryFile="project.exe"

set workingDir=%baseDir%working_directory\

%compilerLocation% %baseDir%%csprojFile% /p:parameters=%1
if not "%1"=="E" (
	if %errorlevel%==0 (
		if not exist %workingDir% ( mkdir %workingDir% )
		cd /D %workingDir%
		%binaryLocation%%binaryFile%
	)
)